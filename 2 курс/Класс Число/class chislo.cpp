﻿#include<iostream>
#include<string>
#include<cmath>
using namespace std;
class Calc {
private:
    int a;
    int b;
    int res=0;
    char operation;
public:
    Calc (int a_, int b_, char operation_) {
        a = a_;
        b = b_;
        operation = operation_;
    }
    void result() {
        switch (operation) {
        case('+'): res = a + b; cout << a << "+" << b << "=" << res << endl; break;
        case('-'): res = a - b; cout << a << "-" << b << "=" << res << endl; break;
        case('*'): res = a * b; cout << a << "*" << b << "=" << res << endl; break;
        case('/'): res = a / b; cout << a << "/" << b << "=" << res << endl; break;
        case('^'): res = pow(a,b); cout << a << "^" << b << "=" << res << endl; break;
        case('<>'):
            if (a < b) { cout << a << "Меньше" << b; break; } 
            if (a > b) { cout << a << "больше" << b; break; } 
            if (a == b) { cout << a << "равны" << b; break; }
        }

    }
};
char operations() {
    char operation;
    while (true) {
        cout << "Укажите выполняемую операцию с введёнными числами('+','-','*','/','^','<>'):";
        cin >> operation;
        if(operation=='+' or operation=='-' or operation == '*' or operation == '/' or operation == '^' or operation == '<>') {
            break;
        }
        else {
            cout << "Некорректная операция.\nПовторите попытку.\n";
        }
        
    }
    return operation;
}
int number() {
    string perevod_system_number;
    while (true) {
        int ch = 0;
        cout << "Укажите систему счисления числа, которое будет вводиться: ";
        cin >> perevod_system_number;
        for (int i = 0; i < perevod_system_number.length(); i++) {
            if (!isdigit(perevod_system_number[i])) {
                ch++;
                break;
            }
        }
        if (ch == 1) {
            cout << "Введенная вами система счисления не является числом.\nПовторите попытку.\n";
        }
        else if (stoi(perevod_system_number) == 2 or stoi(perevod_system_number) == 8 or stoi(perevod_system_number) == 10) {
            cout << "система счисления числа равна: " << stoi(perevod_system_number) << endl;
            break;
        }
        else {
            cout << "система счисления числа не соответствует заданномым системам(2,8,10). Введённая: " << stoi(perevod_system_number) << ".\nПовторите попытку." << endl;
        }
    }
    string num;
    while (true) {
        int ch = 0,ch1=0;
        cout << "Укажите число, в соответствии с введённой системой счисления: ";
        cin >> num;
        for (int i = 0; i < num.length(); i++) {
            if (!isdigit(num[i])) {
                ch++;
                break;
            }
            else if ((num[i] - '0') / stoi(perevod_system_number) != 0) {
                ch1++;
                break;
            }
        }
        if (ch == 1) {
            cout << "Введенное вами число не является числом.\nПовторите попытку." << endl;
        }
        else if (ch1 == 1) {
            cout << "Введённое вами число не соответствует введённой системе счисления\nВведённая система счисления: " << stoi(perevod_system_number) << "\nВведённое число: " << stoi(num)<<endl;
        }
        else {
            cout << "Введённое число: " << num<<endl;
            break;
        }
    }
    int sum = 0;
    for (int i = 0; i < num.length(); i++) {
        sum = sum + ((num[num.length() - i - 1] - '0') * pow(stoi(perevod_system_number), i));
    }
        
    
    return sum;
}
int main() {
    setlocale(LC_ALL, "russian");
    int num1,num2;
    char operation;
    num1 = number();
    num2 = number();
    operation = operations();
    Calc a (num1, num2,operation);
    a.result();
}