class calc:
    def __init__(self,a_,b_,operation_):
        self.a=a_
        self.b=b_
        self.operation=operation_
    def result(self):
        if(self.operation=="+"):
            res=self.a+self.b
            print("{0}+{1}={2}".format(self.a,self.b,res))
        elif (self.operation == "-"):
            res = self.a - self.b
            print("{0}-{1}={2}".format(self.a, self.b, res))
        elif (self.operation == "*"):
            res = self.a * self.b
            print("{0}*{1}={2}".format(self.a, self.b, res))
        elif (self.operation == "/"):
            res = self.a / self.b
            print("{0}/{1}={2}".format(self.a, self.b, res))
        elif (self.operation == "^"):
            res = self.a**self.b
            print("{0}^{1}={2}".format(self.a, self.b, res))
        else:
            if (self.a < self.b):
                print("{0} Меньше {1}".format(self.a,self.b))
            if (self.a > self.b):
                print("{0} Больше {1}".format(self.a, self.b))
            if (self.a == self.b):
                print("{0} Равно {1}".format(self.a, self.b))

def operations():
    while(True):
        operation=input("Укажите выполняемую операцию с введёнными числами('+','-','*','/','^','<>'):")
        if (operation == '+' or operation == '-' or operation == '*' or operation == '/' or operation == '^' or operation == '<>'):
            break
        else:
            print("Некорректная операция.\nПовторите попытку.")
    return operation
def number():
    while(True):
        ch=0
        perevod_system_number=input("Укажите систему счисления вводимого числа: ")
        for i in range(len(perevod_system_number)):
            if(perevod_system_number[i].isdigit()!=1):
                print(perevod_system_number[i])
                ch=1
                break
        if(ch==1):
            print("Введенная вами система счисления не является числом.\nПовторите попытку.\n")
        elif(int(perevod_system_number)==2 or int(perevod_system_number)==8 or int(perevod_system_number)==10):
            print("система счисления числа равна: {0}".format(perevod_system_number))
            break
        else:
            print("система счисления числа не соответствует заданым системам(2,8,10). Введённая:  {0}.\nПовторите попытку.".format(perevod_system_number))

    while (True):
        ch=0
        ch1=0
        num = input("Укажите число, в соответствии с введённой системой счисления: ")
        for i in range(len(num)):
            if (num[i].isdigit() != 1):
                ch = ch+1
                break
            elif (int(num[i])/int(perevod_system_number)>=1):
                ch1 = ch1+1
                break
        if (ch == 1):
            print("Введенное вами число не является числом.\nПовторите попытку.")
        elif (ch1==1):
            print("Введённое вами число не соответствует введённой системе счисления\nВведённая система счисления: {0} \nВведённое число: {1}".format(perevod_system_number,num))
        else:
            sum = 0
            for i in range(len(num)):
                sum = sum + (int(num[len(num) - i - 1]) * (int(perevod_system_number) ** i))
            print("Введённое число: {0}".format(sum))
            break

    return sum

num1=int(number())
num2=int(number())
operation=operations()
a=calc(num1,num2,operation)
print(a.result())