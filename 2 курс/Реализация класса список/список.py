class list():
    def __init__(self,*contain):
        self.contain=[]
        print()
    def push_back(self, item):
        self.contain.append(str(item))
    def pop_back(self):
        if(len(self.contain)==0):
            print("Данный список не содержит никаких элементов для очистки.\nКоманда не выполена.\nУкажите другую команду.")
        else:
            self.contain.pop()
            print("Вы успешно выполнили удаление последнего элемента списка.\nВведите новую команду.")
    def clear(self):
        if(len(self.contain)==0):
            print("Данный список уже является чистым.\nКоманда не выполена.\nУкажите другую команду.")
        else:
            self.contain.clear()
            print("Вы успешно выполнили очистку.\nВведите новую команду.")
    def print(self):
        if(len(self.contain)==0):
            print("Данный список не содержит никаких элементов для вывода.\nКоманда выполена.\nУкажите другую команду.")
        else:
            print("Список:")
            q=1
            for i in self.contain:
                print("{0}. {1}".format(q,i))
                q=q+1
    def reverse(self):
        self.contain=self.contain.reverse()
    def search(self,search):
        q=1
        ch=0
        for i in self.contain:
            if(i==search):
                print("{0}. {1}".format(q, i))
                ch=1
            q=q+1
        if(ch==0):
            print("Данный запрос не найден в списке.\nСкорректируйте запрос или ввелите новую команду.\nКоманда успешно выполнена, поиск произведён.\nВведите новую команду.")
    def sort(self):
        if(len(self.contain)==0):
            print("Данный список не содержит никаких элементов для сортировки.\nКоманда не выполена.\nУкажите другую команду.")
        elif(self.contain.begin()==self.contain.end()):
            print("Данный список содержит один элемент, этого слишком мало для сортировки.\nКоманда не выполнена.\nУкажите другую команду.")
        else:
            self.contain=self.contain.sort()
a=list()
i=1
print("Список команд:")
print(" 1.push_back - добавить элемент в конец списка.")
print(" 2.pop_back - удалить элемент в конце списка.")
print(" 3.clear - полная очистка списка.")
print(" 4.print - вывод списка на экран.")
print(" 5.reverse - преобразование списка в обратном порядке.")
print(" 6.search - поиск элемента по списку.")
print(" 7.sort - сортировка элемнтов по списку.")
while(True):
    command=str(input("Введите команду, которую вы желаете выполнить: "))
    if(command=="push_back"):
        print("Процесс добавления элементов в список не прерывый, при завершении введите команду 'exit'")
        while(True):
            push=str(input("Введите элемент для заполнения списка: {0}.".format(i)))
            if(push=='exit'):
                print("Вы успешно покинули раздел заполнения списка.\nВведите новую команду: ")
                break
            else:
                i=i+1
                a.push_back(push)
    elif(command=="pop_back"):
        a.pop_back()
        i=i-1
    elif(command=="clear"):
        a.clear()
        i=1
    elif(command=="print"):
        a.print()
    elif(command=="reverse"):
        a.reverse()
    elif(command=="search"):
        search=str(input("Введите слово для поиска в списке: "))
        a.search(search)
    elif(command=="sort"):
        a.sort()
    else:
        print("Вы указали неккоректную команду.\nКоманда не выполнена.\nВведите другую команду.")