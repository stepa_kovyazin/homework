﻿#include <iostream>
#include<string>
#include<list>
#include <iterator>
using namespace std;

class List {
private:
list<string>spisok;
public:
	
	void push_back(string item) { spisok.push_back(item); }
	
	void print() {
		if (spisok.empty()) {
			cout << "Данный список не содержит никаких элементов для вывода.\nКоманда выполена.\nУкажите другую команду: ";
		}
		else {
			cout << "Список\n";
			int i = 1;
			list<string>::iterator it = spisok.begin();
			for (it = spisok.begin(); it != spisok.end(); it++) {
				cout << i++ << ". " << *it << endl;

			}
			cout << "Вы успешно вывели список на экран.\nКоманда выполнена.\nУкажите другую команду: ";
		}
	}
	void pop_back() { 
		if (spisok.empty()) {
			cout << "Данный список не содержит никаких элементов для очистки.\nКоманда не выполена.\nУкажите другую команду: ";
		}
		else {
			spisok.pop_back();
			cout << "Вы успешно выполнили удаление последнего элемента списка.\nВведите новую команду: ";
		}
	}
	
	void clear() { 
		if (spisok.empty()) {
			cout << "Данный список уже является чистым.\nКоманда не выполена.\nУкажите другую команду: ";
		}
		else {
			spisok.clear();
			cout << "Вы успешно выполнили очистку.\nВведите новую команду: ";
		}
	}
	
	void reverse() {
		if (spisok.empty()) {
			cout << "Данный список не содержит никаких элементов для преобразования в обратном порядке.\nКоманда не выполена.\nУкажите другую команду: ";
		}
		else if (spisok.begin() == spisok.end()) {
			cout << "Данный список содержит один элемент, этого слишком мало для преобразования в обратном порядке.\nКоманда не выполнена.\nУкажите другую команду: ";
		}
		else {
			spisok.reverse();
			cout << "Вы успешно выполнили преобразование в обратном порядке.\nВведите новую команду: ";
		}
	}

	void search(string search) {
		int ch = 0,i=1;
		list<string>::iterator it = spisok.begin();
		for (it = spisok.begin(); it != spisok.end(); it++) {
			
			if (*it == search) {
				cout <<i<<". " << *it;
				ch++;
			}
			i++;
		}
		if (ch == 0) {
			cout << "Данный запрос не найден в списке.\nСкорректируйте запрос или ввелите новую команду.\nКоманда успешно выполнена, поиск произведён.\nВведите новую команду: ";
		}
	}
	void sort() {
		if (spisok.empty()) {
			cout << "Данный список не содержит никаких элементов для сортировки.\nКоманда не выполена.\nУкажите другую команду: ";
		}
		else if (spisok.begin() == spisok.end()) {
			cout << "Данный список содержит один элемент, этого слишком мало для сортировки.\nКоманда не выполнена.\nУкажите другую команду: ";
		}
		else {
			spisok.sort();
			cout << "Вы успешно выполнили сортировку.\nВведите новую команду: ";
		}
	}
};
int main() {
	setlocale(LC_ALL, "rus");
	cout << "Список команд:"<<endl;
	cout << " 1.push_back - добавить элемент в конец списка.\n";
	cout << " 2.pop_back - удалить элемент в конце списка.\n";
	cout << " 3.clear - полная очистка списка.\n";
	cout << " 4.print - вывод списка на экран.\n";
	cout << " 5.reverse - преобразование списка в обратном порядке.\n";
	cout << " 6.search - поиск элемента по списку.\n";
	cout << " 7.sort - сортировка элемнтов по списку.\n";
	cout << "Введите команду, которую вы желаете выполнить: ";
	while (true) {
		List a;
		int i = 1;
		string command;
		cin >> command;
		if (command == "push_back") {
			cout << "Процесс добавления элементов в список не прерывый, при завершении введите команду 'exit'\n";
			while (true) {
				cout << "Введите элемент для заполнения списка: "<<i++<<".";
				string push;
				cin >> push;
				
				if (push == "exit") {
					i--;
					cout << "Вы успешно покинули раздел заполнения списка.\nВведите новую команду: ";
					break;
				}
				else {
					
					a.push_back(push);
				}
			}
		}
		else if(command=="pop_back") {
			i--;
			a.pop_back();

		}
		else if (command == "clear") {
			i=1;
			a.clear();			
		}
		else if (command == "print") {
			a.print();
		}
		else if (command == "reverse") {
			a.reverse();
		}
		else if (command == "search") {
			string search;
			cout << "Введите слово для поиска";
			cin >> search;
			a.search(search);
		}
		else if (command == "sort") {
			a.sort();
		}
		else {
			cout << "Вы указали неккоректную команду.\nКоманда не выполнена.\nВведите другую команду: ";
		}
	}
}