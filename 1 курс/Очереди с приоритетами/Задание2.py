from math import log
print("Условия задачи: ")
print("Количество обменов сортировки m должно удовлетворять неравенству 0≤m≤4*n ")
print("Правило бинарного дерева: ")
print("выполняются неравенства A[i] ≤ A[2i + 1] и A[i] ≤ A[2i + 2] для всех i.")
#Проверка ввода
while True:
  n=input("Укажите Количество элементов: ")
  try:
    n=int(n)
    break
  except:
    if n.isalpha():
      print("Введены буквы")
    else:
      print("Введены непонятные символа")
arr = []
#Проверка ввода
while True:
  arr1=input("укажите элементы бинарного дерева в строку через пробел: ").split()
  try:     
    if len(arr1)==n:
      for i in arr1:
        arr.append(int(i))
      break
    else:
      print("Вы не указали нужное кол-во элементов")
  except:
    if i.isalpha():
        print("Введены буквы")
    else:
      print("Введено что-то в сочитании с буквами")
#Вывод массива в исходном состоянии
print("Исходные элементы бинарного дерева:" )
for i in range(n):
  if(i<n-1):
    print(arr[i],end=", ")
  else:
    print(arr[i])
#Счётчик перестановок
ch=0
#сортировка
for hi in range (n-1):
  for i in range (n-hi-1):
    if arr[i]>arr[i+1]:
      print(arr[i],"<->",arr[i+1])
      temp=arr[i]
      arr[i]=arr[i+1]
      arr[i+1]=temp  
      ch+=1
#Проверка выполнения условия   
print("Количество обменов: ",ch)
if ch<=4*n:
  print("Алгоритм сортировки справился с условием")
else:
  print("Алгоритм сортировки не справился с условием")
  exit()
print("Отсортированный массив по правилам бинарного дерева: ")
#Вывод сортировки
for i in range(n):
  if(i<n-1):
    print(arr[i],end=", ")
  else:
    print(arr[i])
#Вывод дерева
print("Бинарное дерево по уровням:")


level=1
for i in range(n):
    if(i==0):
      print("0 level:","  |{}|".format(arr[i]) )
      
      if(2*i+2<n):
        print("1 level:"," {}|{},{}|".format(arr[i],arr[2*i+1],arr[2*i+2]))
    else:
      if(2*i+1==n):
        break
      if(log(i+1,2)%1==0):
        
        level=level+1
        print("{} level:".format(level),end='')
      print("  {}|{}".format(arr[i],arr[2*i+1]),end='')
      if(2*i+2==n):
        print("|")
        break
      else:
        print(",{}|".format(arr[2*i+2]),end=' ')
        if(log(i+2,2)%1==0):
          print()
print(end='\n'"Бинарное дерево с номерами каждого элемента:")
print()
level=1
for i in range(n):
    if(i==0):
      print("0 level:","  |{}|".format(i) )
      
      if(2*i+2<n):
        print("1 level:"," {}|{},{}|".format(i,2*i+1,2*i+2))
    else:
      if(2*i+1==n):
        break
      if(log(i+1,2)%1==0):
        
        level=level+1
        print("{} level:".format(level),end='')
      print("  {}|{}".format(i,2*i+1),end='')
      if(2*i+2==n):
        print("|")
        break
      else:
        print(",{}|".format(2*i+2),end=' ')
        if(log(i+2,2)%1==0):
          print()
if(log(i+1,2)%1==0):
  print(end='\n'"Данное бинарное дерево является полным") 
else:
  print(end='\n'"Данное бинарное дерево не является полным")  

