#include <iostream>
using namespace std;
int main() {
  setlocale(LC_ALL, "rus");
  cout << "Список команд:" << endl;
  cout << " - insert – добавляет в очередь элемент с приоритетом x" << endl;
  cout << " - ingetMax – значение максимального приоритета" << endl;
  cout << " - remove – удаление последнего человека из очереди " << endl;
  cout << " - print – вывод на экран текущую очередь с указанными приоритетами" << endl;
  cout << " - exit – выход из программы" << endl;
  string command;
  int human;
  cout << "Укажите количество людей в очереди: ";
  cin >> human;
  while (!human){
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    cout << "Неправильный ввод, возможно вы указали что-то кроме числа, повторите попытку" << endl;
    cout << "Укажите количество людей в очереди: ";
    cin >> human;
  }
  cout << human << " человека в очереди" << endl;
  int arr[human];
  for (int q = 0; q < human; q++) {
    
    cout << "Человек в очереди: " << q + 1 << endl;
    cout << "Введите команду: ";
    
    cin >> command;
    if (command == "insert") {
      int prior=0;
      cout << "Укажите приоритет данному человеку в очереди: ";
      cin >> prior;
      cout << q + 1 << ":" << prior << endl;
      arr[q] = prior;
    }
    else if (command == "ingetMax") {
      int max = arr[0];
      for (int maxq = 0; maxq < q; maxq++) {
        if (max < arr[maxq]) {
          max = arr[maxq];
        }
      }
    cout << "Максимальный приоритет в очереди: " << max << endl;
    q = q - 1;
    }
    else if (command == "remove") {
      q = q - 2;
    }
    else if (command == "print") {
      if (q == 0) {
        cout << "Вы еще не указали ни одного приоритета в очереди" << endl;
        q = q - 1;
      }
      else {
        cout << "Текущая очередь: " << endl;
        int max = arr[0];
        for (int maxq = 0; maxq < q; maxq++) {
          if (max < arr[maxq]) {
            max = arr[maxq];
          }
        }
        for (int prior = 0; prior < max + 1; prior++) {
          for (int i = 0; i < q; i++) {
            if (arr[i] == prior) {
              cout << i + 1 << "." << arr[i] << endl;
            }
          }
        }
          q = q - 1;
      }
    }
    else if (command == "exit") {
      cout << "Вы уверены, что хотите выйти во время работы программы?" << endl;
      cout << "Если да,то введите 'yes'" << endl;
      cout << "Если нет,то введите 'no'" << endl;
      cout << "";
      cin >> command;
      if (command == "no") {
        q = q - 1;
      }
      if (command == "yes") {
        cout << "Текущая очередь: " << endl;
        int max = arr[0];
        for (int maxq = 0; maxq < q; maxq++) {
          if (max < arr[maxq]) {
            max = arr[maxq];
          }
        }
        for (int prior = 0; prior < max + 1; prior++) {
          for (int i = 0; i < q; i++) {
            if (arr[i] == prior) {
              cout << i + 1 << "." << arr[i] << endl;
            }
          }
        }
      return 0;
      }
    }
    else{
      cout<<"Вы ввели неправильную команду, повторите попытку"<<endl;
      q=q-1;
    }
  
  }
  cout << "Распределение очереди закончено" << endl;
  cout << "Доступные команды:" << endl;
  cout << "-print-вывод на экран очередь" << endl;
  cout << "-exit-выход из программы" << endl;
  cout << "Введите команду: ";
  for(int i=0;i<=1;i++){
    cin >> command;
    if (command == "print") {
      int max = arr[0];
      for (int maxq = 0; maxq < human; maxq++) {
        if (max < arr[maxq]) {
          max = arr[maxq];
        }
      }
      for (int prior = 0; prior < max + 1; prior++) {
        for (int q = 0; q < human; q++) {
          if (arr[q] == prior) {
            cout << q + 1 << "." << arr[q] << endl;
          }
        }
      }
      cout << "С вами было приятно иметь дело, всего хорошего" << endl;
    }
    else if (command == "exit") {
      cout << "С вами было приятно иметь дело, всего хорошего" << endl;
      return 0;
    }
    else{
      i=i-1;
      cout<<"Вы указали неверную команду, повторите попытку"<<endl;
    }
  }
}