#include <iostream>
#include <cmath>

using namespace std;

int main()
{
  int n;
  cout<<"Введите число: ";
  cin>>n;
  int ch=0,n1=n,n2=n,n3=n,n4=n;
  
  while(n1>0){
    ch=ch+1;
    n1=n1/3;
  }

  int *arr1=new int[ch];
  for (int i=0;i<ch;i++){
    if(n2>0){   
      arr1[i]=n2%3;
      n2=n2/3;
    }
  }
 
  int temp;
  for(int i=0; i<ch/2;i++){
    temp=arr1[i];
    arr1[i]=arr1[ch-i-1];
    arr1[ch-i-1]=temp;
  }
  cout<<"Троичное число: ";
  for (int i=0;i<ch;i++){
    cout<<arr1[i];
  }
  cout<<endl;

  int *arr2=new int[ch*2];
  for (int i=0;i<ch*2;i++){
    if(i%2==1){
      arr2[i]=arr2[i-1];
    }
    else{
      if(n>0){  
        arr2[i]=n3%3;
        n3=n3/3;
      }
    }
  }

  int dec=0;
  for(int i=0;i<ch*2;i++){
    dec=arr2[i]*pow(3,i)+dec;
  }

  for(int i=0; i<ch;i++){
    temp=arr2[i];
    arr2[i]=arr2[ch*2-i-1];
    arr2[ch*2-i-1]=temp;
  }
  cout<<"Закодированное троичное число: ";
  for (int i=0;i<ch*2;i++){
    cout<<arr2[i]; 
  }

  cout<<endl<<"Закодированное дессятичное число: "<<dec<<endl;
}